package pl.badsoft.edu.event.employee.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import pl.badsoft.edu.event.employee.EmployeeType;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class EmployeeDto {

	private Long id;
	private String firstname;
	private String lastname;
	private String email;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate birthDay;
	private EmployeeType employeeType;
}
