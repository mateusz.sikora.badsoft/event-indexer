# Micro-services application with RabbitMQ and Elasticsearch (multi-module gradle project)

## data-publisher

Run RabbitMQ in Docker:
```
docker pull rabbitmq:3.13.4-management

docker run -d -p 5672:5672 -p 15672:15672 --name event-rabbit -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password rabbitmq:3.13.4-management
```

Run Postgres in Docker:
```
docker pull postgres:16.3

docker run -p 5432:5432 --name=event-db-local --detach -e POSTGRES_PASSWORD=password -e POSTGRES_USER=user -e POSTGRES_DB=event-db-local postgres:16.3
```


## elastic-app

Run Elasticsearch in Docker:
```
docker pull elasticsearch:8.14.3

docker run -d --name event-elastic -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e "xpack.security.enabled=false" elasticsearch:8.14.3
```


