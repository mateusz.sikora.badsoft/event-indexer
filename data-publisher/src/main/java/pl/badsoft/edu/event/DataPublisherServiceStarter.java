package pl.badsoft.edu.event;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
public class DataPublisherServiceStarter {

	public static void main(String[] args) {
		SpringApplication.run(DataPublisherServiceStarter.class, args);
	}
}
