package pl.badsoft.edu.event.employee;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Document(indexName = "employee")
public class Employee {

	@Id
	private String id;

	@Field(type = FieldType.Text, name = "firstname")
	private String firstname;

	@Field(type = FieldType.Text, name = "lastname")
	private String lastname;

	@Field(type = FieldType.Text, name = "email")
	private String email;

	@Field(type = FieldType.Date, name = "birthDay")
	private LocalDate birthDay;

	@Field(type = FieldType.Keyword, name = "employeeType")
	private EmployeeType employeeType;
}
