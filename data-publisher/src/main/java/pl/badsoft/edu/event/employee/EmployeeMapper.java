package pl.badsoft.edu.event.employee;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
interface EmployeeMapper {

	@Mapping(target = "id", ignore = true)
	EmployeeEntity mapToEmployeeEntity(EmployeeDto employeeDto);

	EmployeeDto mapToEmployeeDto(EmployeeEntity entity);
}
