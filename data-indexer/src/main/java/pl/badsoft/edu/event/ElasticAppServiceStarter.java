package pl.badsoft.edu.event;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
@EnableRabbit
@EnableElasticsearchRepositories
public class ElasticAppServiceStarter {

	public static void main(String[] args) {
		SpringApplication.run(ElasticAppServiceStarter.class, args);
	}
}
