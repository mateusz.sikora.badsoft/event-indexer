package pl.badsoft.edu.event.employee;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import pl.badsoft.edu.event.employee.dto.EmployeeDto;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface EmployeeMapper {

	Employee mapToEmployee(EmployeeDto employeeDto);
}
