package pl.badsoft.edu.event.employee.dto;

import java.io.IOException;

import pl.badsoft.edu.event.Config;
import pl.badsoft.edu.event.employee.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import pl.badsoft.edu.event.employee.EmployeeMapper;

@Component
@RequiredArgsConstructor
@Slf4j
public class EventEmployeeListener {

	private final ObjectMapper objectMapper;
	private final EmployeeService employeeService;
	private final EmployeeMapper employeeMapper;

	@RabbitListener(queues = Config.QUEUE_EVENT)
	public void eventHandler(Message message) throws IOException {
		EmployeeDto employeeDto = objectMapper.readValue(message.getBody(), EmployeeDto.class);
		log.info("[SUBSCRIBER] Get event with employee id {}", employeeDto.getEmail());
		employeeService.saveEmployee(employeeMapper.mapToEmployee(employeeDto));
	}
}
