package pl.badsoft.edu.event.employee;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

record EmployeeDto(Long id,
				   String firstname,
				   String lastname,
				   String email,
				   @JsonFormat(pattern = "yyyy-MM-dd")
				   LocalDate birthDay,
				   EmployeeType employeeType) {
}
