package pl.badsoft.edu.event.employee;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
interface EmployeeRepository extends ElasticsearchRepository<Employee, String> {
}
