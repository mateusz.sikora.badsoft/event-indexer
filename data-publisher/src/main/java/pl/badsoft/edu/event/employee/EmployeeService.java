package pl.badsoft.edu.event.employee;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
class EmployeeService {

	private final EmployeeRepository employeeRepository;
	private final EmployeeMapper employeeMapper;

	@Transactional
	public EmployeeDto createEmployee(EmployeeDto employeeDto) {
		EmployeeEntity employeeEntity = employeeRepository.save(employeeMapper.mapToEmployeeEntity(employeeDto));
		return employeeMapper.mapToEmployeeDto(employeeEntity);
	}
}
