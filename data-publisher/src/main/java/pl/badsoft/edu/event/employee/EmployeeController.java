package pl.badsoft.edu.event.employee;

import java.util.Collection;

import pl.badsoft.edu.event.Config;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/employee")
@RequiredArgsConstructor
@Slf4j
class EmployeeController {

	private final RabbitTemplate template;
	private final EmployeeService employeeService;

	@PostMapping
	public ResponseEntity<Void> createEmployee(@RequestBody EmployeeDto employeeDto) {
		log.info("[PUBLISHER] Send event with employee email {}", employeeDto.email());
		EmployeeDto employee = employeeService.createEmployee(employeeDto);
		template.convertAndSend(Config.QUEUE_EVENT, employee);
		return ResponseEntity.ok().build();
	}

	@PostMapping("/list")
	public ResponseEntity<Void> createEmployees(@RequestBody Collection<EmployeeDto> employees) {
		log.info("[PUBLISHER] Send many employees {} event", employees.size());

		employees.forEach(employeeDto -> {
			EmployeeDto employee = employeeService.createEmployee(employeeDto);
			template.convertAndSend(Config.QUEUE_EVENT, employee);
		});
		return ResponseEntity.ok().build();
	}
}
