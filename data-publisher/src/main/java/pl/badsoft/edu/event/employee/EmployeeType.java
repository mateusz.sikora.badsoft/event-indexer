package pl.badsoft.edu.event.employee;

public enum EmployeeType {
	INTERNAL, EXTERNAL
}
